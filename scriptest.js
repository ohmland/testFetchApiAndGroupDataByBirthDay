function createNode(element){
    return document.createElement(element);
}

function append(parent, el){
    return parent.appendChild(el);
}

function setAttr(el, name){
    return el.setAttribute("id", name);
}

const tbody = document.getElementById("tbdy");

const url = 'http://uinames.com/api/?ext&amount=25';
 
fetch(url)
    .then((response) => response.json())
    .then(function(data){
        let users = data;
        let results = users.reduce(
            (stat, user) => {
                let d = new Date(user.birthday.mdy);
                let days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
                let dayName = days[d.getDay()];

                let obj = {};
                obj["photo"] = user.photo;
                obj["name"] = `${user.name} ${user.surname}`;
                
                if(stat[dayName]){
                    stat[dayName].push(obj);
                }else{
                    stat[dayName] = [obj];
                }
                return stat
        },{}
    )

    let sorter = {
        "SUN": 1,
        "MON": 2,
        "TUE": 3,
        "WED": 4,
        "THU": 5,
        "FRI": 6,
        "SAT": 7
    };
    
    let tmp = [];
    Object.keys(results).forEach(function(key) {
        let value = results[key];
        let index = sorter[key];
        tmp[index] = {
            key: key,
            value: value
        };
    });
    
    var orderedData = {};
    tmp.forEach(function(obj) {
        orderedData[obj.key] = obj.value;
    });
    
    console.log('>>>>', orderedData);
    
    let maxRow = 0;
    for (let key in orderedData) {
        let i = 0;
        if(maxRow < orderedData[key].length){
            maxRow = orderedData[key].length;
        }
        i++;
    }
    console.log('maxRow', maxRow);
    
    for (let i = 0; i < maxRow; i++) {
        let tr = createNode('tr'); //วนสร้างแถวตามจำนวนของ maxRow
        setAttr(tr,'tr'+i);//สร้าง id ให้แต่ละแถว สรุปไม่ต้องใช้ --function นี้เก็บเอาไว้ set attribute tooltip ได้
        
        for (let key in orderedData) {
            let td,img,div;
            switch (key) {
                case "SUN":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                case "MON":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                case "TUE":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                case "WED":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                case "THU":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                case "FRI":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                case  "SAT":
                    td = createNode('td')
                    if(orderedData[key][i]){
                        img = createNode('img'),
                        div = createNode('div')
                        img.src = orderedData[key][i].photo;
                        div.innerHTML = `${orderedData[key][i].name}`;
                        append(td,img);
                        append(td,div);
                    }
                    append(tr,td);
                    break;
                }
                append(tbody,tr);
            }
    }
})  
.catch(function(error){
    console.log(error);
});